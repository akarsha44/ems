// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBhQ1veG2JEMSykGT33KYRjaJuvekueMxQ',
    authDomain: 'tokenservice-405a4.firebaseapp.com',
    projectId: 'tokenservice-405a4',
    storageBucket: 'tokenservice-405a4.appspot.com',
    messagingSenderId: '582700831160',
    appId: '1:582700831160:web:901b8710c7cfc407133f4d',
    measurementId: 'G-D9GZFQFB49',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
